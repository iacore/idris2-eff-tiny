module Control.FreeTiny

%default total

public export
data Free : (Type -> Type) -> Type -> Type where
    Pure : a -> Free m a
    Bind : m a -> (a -> Free m b) -> Free m b


export
mapK : (forall b. m b -> n b) -> Free m a -> Free n a
mapK fmap (Pure val) = Pure val
mapK fmap (Bind mval mcont) = Bind (fmap mval) (\a => (mapK fmap (mcont a)))

export
recurbind : Free m a -> (a -> Free m b) -> Free m b
recurbind (Pure val) cont = cont val
recurbind (Bind val vcont) cont = Bind val (\val' => recurbind (vcont val') cont)

export
lift : m a -> Free m a
lift mval = Bind mval Pure

export
flatten : Monad m => Free m a -> m a
flatten (Pure val) = pure val
flatten (Bind mval mcont) = mval >>= (\val' => flatten $ mcont val')

export
Functor (Free m) where
    map f mval = recurbind mval (\xa => Pure (f xa))

export
Applicative (Free m) where
    pure x = Pure x
    (<*>) mf mval = recurbind mf (\f => map f mval)

export
Monad (Free m) where
    (>>=) = recurbind
