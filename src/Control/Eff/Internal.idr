module Control.Eff.Internal

-- import public Control.MonadRec
import public Control.FreeTiny
import public Data.Union
import public Data.Subset

%default total

||| An effectful computation yielding a value
||| of type `t` and supporting the effects listed
||| in `fs`.
public export
Eff : (fs : List (Type -> Type)) -> (t : Type) -> Type
Eff fs = Free (Union fs)

||| Lift a an effectful comutation into the `Eff` monad.
export
send : Has f fs => f t -> Eff fs t
send @{has} x = lift (inj @{has} x)

||| Handle all effectful computations in `m`,
||| returning the underlying free monad.
export
toFree : Handler m fs -> Eff fs t -> Free m t
toFree h eff = mapK (handleAll h) eff

||| Run an effectful computation without overflowing
||| the stack by handling all computations in monad `m`.
export
runEff : Monad m => Handler m fs -> Eff fs t -> m t
runEff h eff = flatten $ mapK (handleAll h) eff

||| Extract the (pure) result of an effectful computation
||| where all effects have been handled.
export
extract : Eff [] a -> a
extract (Pure val) = val
extract (Bind u _) = absurd u


||| Handle effect and transform result into another type
||| @f effect to be handled
export
handleRelay : (prf : Has f fs)
  => (a -> Eff (fs - f) b)
  -> (forall v . f v -> (v -> Eff (fs - f) b) -> Eff (fs - f) b)
  -> Eff fs a
  -> Eff (fs - f) b
handleRelay fval fcont (Pure val) = fval val
handleRelay fval fcont (Bind x g) =
  case decomp {prf} x of
    Left y  => lift y >>= (\x' => handleRelay fval fcont (g x'))
    Right y => fcont y (\x' => handleRelay fval fcont (g x'))


||| handle effect with state
export
handleRelayS :  (prf : Has f fs)
             => s
             -> (s -> a -> Eff (fs - f) b)
             -> (forall v . s -> f v -> (s -> v -> Eff (fs - f) b) -> Eff (fs - f) b)
             -> Eff fs a
             -> Eff (fs - f) b
handleRelayS vs fval fcont fr = case fr of
  Pure val => fval vs val
  Bind x g => case decomp {prf} x of
    Left y  => lift y >>= (\val' => handleRelayS vs fval fcont (g val'))
    Right y => fcont vs y (\vs2, val' => handleRelayS vs2 fval fcont (g val'))


||| handle effect without transforming result type
export
handle : (prf : Has f fs)
      => (forall v . f v -> (resume: v -> Eff (fs - f) b) -> Eff (fs - f) b)
      -> Eff fs b
      -> Eff (fs - f) b
handle fcont fr = handleRelay pure fcont fr


||| handle effect without transforming result type or the ability to abort computation (thus "linear")
export
handleLinear : (prf : Has f fs)
      => (forall v . f v -> Eff (fs - f) v)
      -> Eff fs b
      -> Eff (fs - f) b
handleLinear handler (Pure val) = Pure val
handleLinear handler (Bind mval mcont) = do
  let mcont' = \val' => (handleLinear handler (mcont val'))
  case decomp {prf} mval of
        Left mval'  => Bind mval' mcont'
        Right valx => (handler valx) >>= mcont'


||| Add one effect into effect union.
export
lift1 : Eff fs a -> Eff (f :: fs) a
lift1 (Pure val) = pure val
lift1 (Bind x g) = do
  let mx = weaken1 x
  freex <- lift mx
  lift1 (g freex)


||| Turn effect monad into a more relaxed one. Can be used to reorder effects as well. See src/Test/Ordering.idr for usage.
export
lift : Subset fs fs' => Eff fs a -> Eff fs' a
lift @{s} (Pure val) = pure val
lift @{s} (Bind x g) = do
  let mx = weaken @{s} x
  freex <- lift mx
  lift (g freex)
